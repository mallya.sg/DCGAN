from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from six.moves import urllib
import os
import re
import sys
import numpy as np

import tensorflow as tf

class batch_normalization(object):
  """Code modification of http://stackoverflow.com/a/33950177"""
  def __init__(self, epsilon=1e-5, momentum = 0.9, name="batch_norm"):
    with tf.variable_scope(name):
      self.epsilon = epsilon
      self.momentum = momentum

      self.ema = tf.train.ExponentialMovingAverage(decay=self.momentum)
      self.name = name

  def __call__(self, x, train=True):
    return tf.contrib.layers.batch_norm(x, decay=self.momentum, updates_collections=None, epsilon=self.epsilon, scale=True, scope=self.name, is_training=train)

class DCGAN(object):
  def __init__(self, FLAGS):
    self.FLAGS = FLAGS

  def activation_summary(self, x):
    """
      Helper to create summaries for activations.

      Creates a summary that provides a histogram of activations.
      Creates a summary that measures the sparsity of activations.

      Args:
        x: Tensor

      Returns:
        nothing
    """
    # Remove the 'tower_[0-9]/' from the name in case this is a multi-GPU training session.
    # This helps the clarity of presentation on tensorboard.
    tensor_name = re.sub('%s_[0-9]*/' % self.FLAGS.tower_name, '', x.op.name)
    tf.summary.histogram(tensor_name + '/activations', x)
    tf.summary.scalar(tensor_name + '/sparsity', tf.nn.zero_fraction(x))

  def variable_on_cpu(self, name, shape, initializer):
    """
      Helper to create variable on CPU memory.

      Args:
        name: Name of the variable
        shape: list of ints
        initilizer: initializer for variable

        Returns:
          Variable tensor
    """
    with tf.device('/cpu:0'):
      dtype = tf.float16 if self.FLAGS.use_fp16 else tf.float32
      var = tf.get_variable(name, shape, initializer=initializer, dtype=dtype)

    return var
  def variable_with_weight_decay(self, name, shape, stddev, wd, prefix='Generator'):
    """
      Helper to create an initialized variable with weight decay.

      Note that variable is intialized with a truncated normal distribution.
      A weight decay is added only if one is specified.

      Args:
        name: Name of the variable
        shape: list of ints
        stddev: Standard deviation of truncated Gaussian
        wd: Add a L2 Loss weight decay multiplied by this float.
            If None, weight decay is not added for this variable

      Returns:
        Variable Tensor
    """
    dtype = tf.float16 if self.FLAGS.use_fp16 else tf.float32
    var = self.variable_on_cpu(name, shape, tf.truncated_normal_initializer(
      stddev=stddev, dtype=dtype))

    if wd is not None:
      weight_decay = tf.multiply(tf.nn.l2_loss(var), wd, name='weight_loss')
      tf.add_to_collection(prefix + '/losses', weight_decay)

    return var

  def leaky_ReLU(self, x, leak=0.2, name='leaky_ReLU'):
    """
      Implementes leaky ReLU
      Args:
        x :
        leak :
    """
    f1 = 0.5 * (1 + leak)
    f2 = 0.5 * (1 - leak)

    return f1 * x + f2 * abs(x)

  def batch_norm(self, x, n_out, is_training, scope_name='bn'):
    """
    Batch normalization on convolutional maps.
    Ref.:   
    Args:
        x:           Tensor, 4D BHWD input maps
        n_out:       integer, depth of input maps
        phase_train: boolean tf.Variable, true indicates training phase
        scope:       string, variable scope
    Return:
        normed:      batch-normalized maps
    """
    if is_training:
      is_training_mode = tf.Variable(True, name=scope_name + '/is_training_mode')
    else:
      is_training_mode = tf.Variable(False, name=scope_name + '/is_not_training_mode')

    with tf.variable_scope(scope_name) as scope:
      beta = self.variable_on_cpu('beta', [n_out], tf.constant_initializer(0.0))
      gamma = self.variable_on_cpu('gamma', [n_out], tf.constant_initializer(1.0))
      batch_mean, batch_var = tf.nn.moments(x, [0,1,2], name='moments')
      print(batch_mean.name)
      ema = tf.train.ExponentialMovingAverage(decay=0.9, name='EMA')

      def mean_var_with_update():
        ema_apply_op = ema.apply([batch_mean, batch_var])
        with tf.control_dependencies([ema_apply_op]):
          return tf.identity(batch_mean), tf.identity(batch_var)

      mean, var = tf.cond(is_training_mode,
        mean_var_with_update,
        lambda: (ema.average(batch_mean), ema.average(batch_var)))

      normed = tf.nn.batch_normalization(x, mean, var, beta, gamma, 1e-3, name=scope.name)

    return normed

  def generator(self, z, is_training, reuse=False):
    """
      Build the generator model.
    """
    with tf.variable_scope("Generator_Variable_Scope") as scope_super:
      if reuse:
        scope_super.reuse_variables()
      #Local1
      with tf.variable_scope('local1') as scope:
        dim = self.FLAGS.random_input_size
        weights = self.variable_with_weight_decay('weights',
                                                  shape=[dim, 1024 * 4 * 4],
                                                  stddev=0.02, wd=None, prefix='Generator')
      
        biases = self.variable_on_cpu('biases', [1024 * 4 * 4], tf.constant_initializer(0.0))
        local1 = tf.nn.bias_add(tf.matmul(z, weights), biases, name=scope.name)
      
        self.activation_summary(local1)
      
      #Transposed Conv2
      with tf.variable_scope('local1_reshaped') as scope:  
        local1_reshaped = tf.reshape(local1, [self.FLAGS.batch_size, 4, 4, 1024], name=scope.name)
      with tf.variable_scope('transposed_conv2') as scope:
        kernel = self.variable_with_weight_decay('weights',
                                                 shape=[5, 5, 512, 1024],
                                                 stddev=2e-2,
                                                 wd=None,
                                                 prefix='Generator')
      
        transposed_conv = tf.nn.conv2d_transpose(local1_reshaped,
                                                 kernel,
                                                 output_shape=[self.FLAGS.batch_size, 8, 8, 512],
                                                 strides=[1, 2, 2, 1])
      
        biases = self.variable_on_cpu('biases', [512], tf.constant_initializer(0.0))
        # pre_activation = tf.nn.bias_add(transposed_conv, biases)
        # transposed_conv2 = tf.nn.relu(pre_activation, name=scope.name)
        transposed_conv2 = tf.nn.bias_add(transposed_conv, biases, name=scope.name)
        self.activation_summary(transposed_conv2)
      
      #Batch Normalized2
      with tf.variable_scope('bn2') as scope:
        bn2 = tf.contrib.layers.batch_norm(
          transposed_conv2,
          center=True,
          scale=True,
          updates_collections=None,
          is_training=is_training,
          scope=scope,
          epsilon=1e-5,
          decay=0.9
        )
      # ReLU2
      relu2 = tf.nn.relu(bn2, name='relu2')
        
      #Transposed Conv3
      with tf.variable_scope('transposed_conv3') as scope:
        kernel = self.variable_with_weight_decay('weights',
                                                 shape=[5, 5, 256, 512],
                                                 stddev=2e-2,
                                                 wd=None,
                                                 prefix='Generator')
      
        transposed_conv = tf.nn.conv2d_transpose(relu2,
                                                 kernel,
                                                 output_shape=[self.FLAGS.batch_size, 16, 16, 256],
                                                 strides=[1, 2, 2, 1])
      
        biases = self.variable_on_cpu('biases', [256], tf.constant_initializer(0.0))
        # pre_activation = tf.nn.bias_add(transposed_conv, biases)
        # transposed_conv3 = tf.nn.relu(pre_activation, name=scope.name)
        transposed_conv3 = tf.nn.bias_add(transposed_conv, biases, name=scope.name)
        self.activation_summary(transposed_conv3)
      
      #Batch Normalized3
      with tf.variable_scope('bn3') as scope:
        bn3 = tf.contrib.layers.batch_norm(
          transposed_conv3,
          center=True,
          scale=True,
          updates_collections=None,
          is_training=is_training,
          scope=scope,
          epsilon=1e-5,
          decay=0.9
        )
      
      # ReLU3
      relu3 = tf.nn.relu(bn3, name='relu3')
      
      #Transposed Conv4
      with tf.variable_scope('transposed_conv4') as scope:
        kernel = self.variable_with_weight_decay('weights',
                                                 shape=[5, 5, 128, 256],
                                                 stddev=2e-2,
                                                 wd=None,
                                                 prefix='Generator')
      
        transposed_conv = tf.nn.conv2d_transpose(relu3,
                                                 kernel,
                                                 output_shape=[self.FLAGS.batch_size, 32, 32, 128],
                                                 strides=[1, 2, 2, 1])
      
        biases = self.variable_on_cpu('biases', [128], tf.constant_initializer(0.0))
        # pre_activation = tf.nn.bias_add(transposed_conv, biases)
        # transposed_conv4 = tf.nn.relu(pre_activation, name=scope.name)
        transposed_conv4 = tf.nn.bias_add(transposed_conv, biases, name=scope.name)
        self.activation_summary(transposed_conv4)
      
      ## #Batch Normalized4
      with tf.variable_scope('bn4') as scope:
        bn4 = tf.contrib.layers.batch_norm(
          transposed_conv4,
          center=True,
          scale=True,
          updates_collections=None,
          is_training=is_training,
          scope=scope,
          epsilon=1e-5,
          decay=0.9
        )
      # ReLU4
      relu4 = tf.nn.relu(bn4, name='relu4')
      
      #Output
      with tf.variable_scope('output') as scope:
        kernel = self.variable_with_weight_decay('weights',
                                                 shape=[5, 5, 3, 128],
                                                 stddev=2e-2,
                                                 wd=None,
                                                 prefix='Generator')
      
        transposed_conv = tf.nn.conv2d_transpose(relu4,
                                                 kernel,
                                                 output_shape=[self.FLAGS.batch_size, 64, 64, 3],
                                                 strides=[1, 2, 2, 1])
      
        biases = self.variable_on_cpu('biases', [3], tf.constant_initializer(0.0))
        pre_activation = tf.nn.bias_add(transposed_conv, biases)
        output = tf.nn.tanh(pre_activation, name=scope.name)
        self.activation_summary(output)

    return output

  def discriminator(self, images, is_training, reuse=False):
    """
      Build the discriminator model.
      Args:
        images : Images
    """
    # if is_training:
    #   is_training_mode = tf.Variable(True, name='Discriminator/is_training_mode')
    # else:
    #   is_training_mode = tf.Variable(True, name='Discriminator/is_training_mode')
    print('reuse : {}'.format(reuse))
    with tf.variable_scope("Discriminator_Variable_Scope") as scope_super:
      if reuse:
        scope_super.reuse_variables()

      #Conv1
      with tf.variable_scope('conv1') as scope:
        kernel = self.variable_with_weight_decay('weights',
                                                 shape=[5, 5, 3, 64],
                                                 stddev=2e-2,
                                                 wd=None,
                                                 prefix='Discriminator')

        conv = tf.nn.conv2d(images, kernel, [1, 2, 2, 1], padding='SAME')
        biases = self.variable_on_cpu('biases', [64], tf.constant_initializer(0.0))
        conv1 = tf.nn.bias_add(conv, biases, name=scope.name)
        self.activation_summary(conv1)

      # No Batch Normalization for the first layer
      # Leaky ReLU1
      with tf.variable_scope('lReLU1') as scope:
        lReLU1 = self.leaky_ReLU(conv1, leak=0.2, name=scope.name)

      #Conv2
      with tf.variable_scope('conv2') as scope:
        kernel = self.variable_with_weight_decay('weights',
                                                 shape=[5, 5, 64, 128],
                                                 stddev=2e-2,
                                                 wd=None,
                                                 prefix='Discriminator')
        conv = tf.nn.conv2d(lReLU1, kernel, [1, 2, 2, 1], padding='SAME')
        biases = self.variable_on_cpu('biases', [128], tf.constant_initializer(0.0))
        conv2 = tf.nn.bias_add(conv, biases, name=scope.name)
        self.activation_summary(conv2)

      #Batch Normalization 2
      with tf.variable_scope('bn2') as scope:
        bn2 = tf.contrib.layers.batch_norm(
          conv2,
          center=True,
          scale=True,
          updates_collections=None,
          is_training=is_training,
          scope=scope,
          epsilon=1e-5,
          decay=0.9
        )

      with tf.variable_scope('lReLU2') as scope:
        lReLU2 = self.leaky_ReLU(bn2, leak=0.2, name=scope.name)

      #Conv3
      with tf.variable_scope('conv3') as scope:
        kernel = self.variable_with_weight_decay('weights',
                                                 shape=[5, 5, 128, 256],
                                                 stddev=2e-2,
                                                 wd=None,
                                                 prefix='Discriminator')
        conv = tf.nn.conv2d(lReLU2, kernel, [1, 2, 2, 1], padding='SAME')
        biases = self.variable_on_cpu('biases', [256], tf.constant_initializer(0.0))
        conv3 = tf.nn.bias_add(conv, biases, name=scope.name)
        self.activation_summary(conv3)

      #Batch Normalization 3
      with tf.variable_scope('bn3') as scope:
        bn3 = tf.contrib.layers.batch_norm(
          conv3,
          center=True,
          scale=True,
          updates_collections=None,
          is_training=is_training,
          scope=scope,
          epsilon=1e-5,
          decay=0.9
        )

      #Leaky ReLU3
      with tf.variable_scope('lReLU3') as scope:
        lReLU3 = self.leaky_ReLU(bn3, leak=0.2, name=scope.name)

      #Conv4
      with tf.variable_scope('conv4') as scope:
        kernel = self.variable_with_weight_decay('weights',
                                                 shape=[5, 5, 256, 512],
                                                 stddev=2e-2,
                                                 wd=None,
                                                 prefix='Discriminator')
        
        conv = tf.nn.conv2d(lReLU3, kernel, [1, 2, 2, 1], padding='SAME')
        biases = self.variable_on_cpu('biases', [512], tf.constant_initializer(0.0))
        conv4 = tf.nn.bias_add(conv, biases, name=scope.name)
        self.activation_summary(conv4)

      #Batch Normalization 4
      with tf.variable_scope('bn4') as scope:
        bn4 = tf.contrib.layers.batch_norm(
          conv4,
          center=True,
          scale=True,
          updates_collections=None,
          is_training=is_training,
          scope=scope,
          epsilon=1e-5,
          decay=0.9
        )
 
      #Leaky ReLU4
      with tf.variable_scope('lReLU4') as scope:
        lReLU4 = self.leaky_ReLU(bn4, leak=0.2, name=scope.name)

      # local5
      with tf.variable_scope('output') as scope:
        # Move everything into depth so we can perform a single matrix multiply.
        reshape = tf.reshape(lReLU4, [self.FLAGS.batch_size, -1])
        dim = reshape.get_shape()[1].value
        weights = self.variable_with_weight_decay('weights', shape=[dim, 1],
                                                  stddev=0.02, wd=None,
                                                  prefix='Discriminator')
        biases = self.variable_on_cpu('biases', [1], tf.constant_initializer(0.0))
        local5 = tf.nn.bias_add(tf.matmul(reshape, weights), biases, name='local5')
        output = tf.nn.sigmoid(local5, name=scope.name)
        self.activation_summary(output)

    return output

  def inference(self, z, is_training, images = None, generator_reuse=False):
    """
      Build DCGAN model.
      Args:
        images : Images
    """
    generatorOutput = self.generator(z, is_training, reuse=generator_reuse)
    if generator_reuse:
      # discriminatorTrueOutput = self.discriminator(images, is_training, reuse=True)
      discriminatorFalseOutput = self.discriminator(generatorOutput, is_training, reuse=True)
      return generatorOutput, discriminatorFalseOutput
    else:
      discriminatorTrueOutput = self.discriminator(images, is_training, reuse=False)
      discriminatorFalseOutput = self.discriminator(generatorOutput, is_training, reuse=True)
      return generatorOutput, discriminatorTrueOutput, discriminatorFalseOutput

  def loss(self, discriminatorFalseOutput, discriminatorTrueOutput=None, generator_reuse=False):

    if generator_reuse:
      generator_loss = -tf.reduce_mean(tf.log(discriminatorFalseOutput),
                                       name='Generato_Variable_Scope/Total_Loss')
      tf.summary.scalar('Generator_Variable_Scope/Generator_Loss_Summary', generator_loss)
      return generator_loss
    else:
      discriminator_loss = -tf.reduce_mean(
        tf.log(discriminatorTrueOutput) + tf.log(1.0 - discriminatorFalseOutput),
        name='Discriminator_Variable_Scope/Total_Loss'
      )

      generator_loss = -tf.reduce_mean(tf.log(discriminatorFalseOutput),
                                       name='Generato_Variable_Scope/Total_Loss')

      tf.summary.scalar('Discriminator_Variable_Scope/Discriminator_Loss_Summary',
                        discriminator_loss)
      tf.summary.scalar('Generator_Variable_Scope/Generator_Loss_Summary', generator_loss)

      return generator_loss, discriminator_loss

  def add_loss_summaries(self, total_loss, prefix='Generator'):
    """
      Add summaries for losses in DCGAN Model.

      Generates moving average for all the losses and associated summaries for visualizing the
      performanve of the network.

      Args:
        total_loss: Total loss from loss()
      Returns:
        loss_averages_op: op for generating moving averages of losses.
    """
    # Compute the moving average of all individual losses and the total loss.
    loss_averages = tf.train.ExponentialMovingAverage(0.9, name=prefix + '/avg')
    losses = tf.get_collection(prefix + '/losses')
    loss_averages_op = loss_averages.apply([total_loss])

    # Attach a scalar summary to all individual losses and the total loss; do the same for
    # averged version of the losses
    for l in [total_loss]:
      # Name each loss as '(raw)' and name the moving average version of the loss
      # as the original loss name.
      print('loss name : {}'.format(l.op.name))
      tf.summary.scalar(l.op.name + '/raw', l)
      tf.summary.scalar(l.op.name, loss_averages.average(l))

    return loss_averages_op

  def train(self, generator_total_loss, global_step, reuse_optimizer=False, discriminator_total_loss=None):
    """
      Train the DCGAN model.
      Create two optimizers and apply to all trainable variables. Add moving average for all
      trainable variables.

      Args:
        generator_total_loss: Total loss of generator from loss()
        discriminator_total_loss: Total loss of discriminator from loss()
        global_step: Integer variable counting the number of training steps processed.
     Returns:
        generator_train_op: op for training generator
        discriminator_train_op: op for training discriminator
    """
    with tf.variable_scope("Training") as scope:
      if reuse_optimizer:
        scope.reuse_variables()
      # Variables that affect learning rate
      num_batches_per_epoch = self.FLAGS.num_examples_per_epoch_for_training / self.FLAGS.batch_size
      decay_steps = int(num_batches_per_epoch * self.FLAGS.num_epochs_per_decay)

      # Decay the learning rate exponentially based on the number of steps
      d_lr = tf.train.exponential_decay(self.FLAGS.initial_discriminator_learning_rate,
                                        global_step,
                                        decay_steps,
                                        self.FLAGS.learning_rate_decay_factor,
                                        staircase=True)
      tf.summary.scalar('learning_rate_discriminator', d_lr)
        

      g_lr = tf.train.exponential_decay(self.FLAGS.initial_generator_learning_rate,
                                        global_step,
                                        decay_steps,
                                        self.FLAGS.learning_rate_decay_factor,
                                        staircase=True)
      tf.summary.scalar('learning_rate_generator', g_lr)

      trainable_variable_list = tf.trainable_variables()
      
      generator_tv_list = [x for x in trainable_variable_list if 'Generator' in x.name]
      discriminator_tv_list = [x for x in trainable_variable_list if 'Discriminator' in x.name]

      """
      print('List of generator trainable variables : ')
      for x in generator_tv_list:
        print('\t%s' %x.name)

      print('List of discriminator trainable variables : ')
      for x in discriminator_tv_list:
        print('\t%s' %x.name)
      """
      gen_opt = tf.train.AdamOptimizer(self.FLAGS.initial_generator_learning_rate,
                                       self.FLAGS.discriminator_beta1,
                                       name='Generator/AdamOptimizer')
      generator_train_op = gen_opt.minimize(generator_total_loss,
                                            var_list=generator_tv_list,
                                            name='generator_train',
                                            global_step=global_step)

    if reuse_optimizer:
      return generator_train_op
    else:
      dis_opt = tf.train.AdamOptimizer(self.FLAGS.initial_discriminator_learning_rate,
                                       self.FLAGS.discriminator_beta1,
                                       name='Discriminator/AdamOptimizer')
      
      discriminator_train_op = dis_opt.minimize(discriminator_total_loss,
                                                var_list=discriminator_tv_list,
                                                name='discriminator_train',
                                                global_step=global_step)
      return generator_train_op, discriminator_train_op
