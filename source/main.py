from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import time

from DCGANData import DCGANData
from DCGAN import DCGAN

import itertools
from glob import glob
import tensorflow as tf
from six.moves import xrange
import os
import cv2
import numpy as np
import scipy.misc

SUPPORTED_EXTENSIONS = ["png", "jpg", "jpeg"]

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_boolean('train', False,
                            """Boolean variable indicating training or not.""")

tf.app.flags.DEFINE_string('train_dir', '../data/',
                           """Directory containing data for training""")

tf.app.flags.DEFINE_string('checkpoint_dir', '../log/log_July21_2017/',
                           """Directory where to write event logs """
                           """and checkpoint.""")

tf.app.flags.DEFINE_string('eval_dir', '../eval_log/',
                           """Directory where to write event logs and checkpoint.""")

tf.app.flags.DEFINE_integer('eval_interval_secs', 60 * 5,
                            """How often to run the eval.""")

tf.app.flags.DEFINE_boolean('run_once', True,
                            """Whether to run eval only once.""")

tf.app.flags.DEFINE_integer('max_steps', 25 * 205 * 2,
                            """Number of batches to run.""")

tf.app.flags.DEFINE_boolean('log_device_placement', False,
                            """Whether to log device placement.""")

tf.app.flags.DEFINE_integer('log_frequency', 10,
                            """How often to log results to the console.""")

tf.app.flags.DEFINE_integer('batch_size', 64,
                            """Number of images to process in batch.""")

tf.app.flags.DEFINE_float('min_fraction_of_examples_in_queue', 0.8,
                          """Minimum fraction of examples in queue""")

tf.app.flags.DEFINE_string('data_dir', '../data/lfw/',
                           """Path to the LFW data directory.""")

tf.app.flags.DEFINE_string('image_dir', 'aligned',
                           """LFW image directory name.""")


tf.app.flags.DEFINE_boolean('use_fp16', False, """Train the model using fp16.""")

tf.app.flags.DEFINE_integer('image_height', 64, """Height of the input image""")

tf.app.flags.DEFINE_integer('image_width', 64, """Width of the input image""")

tf.app.flags.DEFINE_integer('image_depth', 3, """Depth of the input image""")

tf.app.flags.DEFINE_integer('random_input_size', 100,
                            """Dimension of the random inputs fed to generator""")

tf.app.flags.DEFINE_string('train_file', 'train.tfrecords', """Training data""")
tf.app.flags.DEFINE_string('validation_file', 'val.tfrecords', """Validation data""")
tf.app.flags.DEFINE_string('test_file', 'test.tfrecords', """Test data""")
tf.app.flags.DEFINE_string('dataset', '../data/lfw/aligned/', """Path to aligned data.""")

# Constants describing the training process
tf.app.flags.DEFINE_float('moving_average_decay', 0.9999,
                          """The decay used for moving average""")

tf.app.flags.DEFINE_integer('num_epochs_per_decay', 5000,
                            """Epochs after which learning rate decays""")

tf.app.flags.DEFINE_float('learning_rate_decay_factor', 0.1,
                          """Learning rate decay factor""")

tf.app.flags.DEFINE_float('initial_discriminator_learning_rate', 0.0002,
                          """Initial Learning rate""")

tf.app.flags.DEFINE_float('initial_generator_learning_rate', 0.0002,
                          """Initial Learning rate""")


tf.app.flags.DEFINE_integer('num_examples_per_epoch_for_training', 13175,
                            """Number of examples to train per epoch""")

tf.app.flags.DEFINE_integer('num_epochs', 25, """Number epochs to train.""")

tf.app.flags.DEFINE_string('tower_name', 'tower',
                           """Tower Name in case of multi GPU training""")

tf.app.flags.DEFINE_float('generator_beta1', 0.5,
                          """Momentum term for generator adam optimizer""")

tf.app.flags.DEFINE_float('discriminator_beta1', 0.5,
                          """Momentum term for discriminator adam optimizer""")

get_stddev = lambda x, k_h, k_w: 1/math.sqrt(k_w*k_h*x.get_shape()[-1])

def get_image(image_path, image_size, is_crop=True):
    return transform(imread(image_path), image_size, is_crop)

def imread(path):
    return scipy.misc.imread(path, mode='RGB').astype(np.float)

def center_crop(x, crop_h, crop_w=None, resize_w=64):
    if crop_w is None:
        crop_w = crop_h
    h, w = x.shape[:2]
    j = int(round((h - crop_h)/2.))
    i = int(round((w - crop_w)/2.))
    return scipy.misc.imresize(x[j:j+crop_h, i:i+crop_w],
                               [resize_w, resize_w])

def transform(image, npx=64, is_crop=True):
    # npx : # of pixels width/height of image
    if is_crop:
        cropped_image = center_crop(image, npx)
    else:
        cropped_image = image
    return np.array(cropped_image)/127.5 - 1.

def inverse_transform(images):
  return (images+1.)/2.

def merge(images, size):
    h, w = images.shape[1], images.shape[2]
    img = np.zeros((int(h * size[0]), int(w * size[1]), 3))
    # print(images.shape)
    for idx, image in enumerate(images):
        i = idx % size[1]
        j = idx // size[1]
        # print(image.shape)
        img[j*h:j*h+h, i*w:i*w+w, :] = image
    return img

def imsave(images, size, path):
  img = merge(images, size)
  return scipy.misc.imsave(path, (255*img).astype(np.uint8))

def save_images(images, size, image_path):
  return imsave(inverse_transform(images), size, image_path)

def dataset_files(root):
  """Returns a list of all image files in the given directory"""
  return list(itertools.chain.from_iterable(
    glob(os.path.join(root, "*.{}".format(ext))) for ext in SUPPORTED_EXTENSIONS))
  
def train(data, model):
  """
    Train Segnet for a number of steps.
  """
  with tf.Graph().as_default() as g:
    global_step = tf.contrib.framework.get_or_create_global_step()
    z = tf.placeholder(tf.float32, [None, FLAGS.random_input_size], name='z2')
    images = tf.placeholder(tf.float32, [FLAGS.batch_size, FLAGS.image_width, FLAGS.image_height, FLAGS.image_depth], name='real_images')
    # Get images and z for DCGAN
    # images, z = data.training_inputs()
    # images = tf.placeholder(tf.float32, \
    #                         [None, FLAGS.image_height, FLAGS.image_width, FLAGS.image_depth], \
    #                         name='real_images')
    # z = tf.placeholder(tf.float32, [None, self.z_dim], name='z')
    # z_sum = tf.summary.histogram("z", z)
    is_training = tf.constant(True, name='is_training')
    # Build a graph that computes the logits predictions from the inference model
    g_output, dTrueLogit, dFalseLogit = model.inference(images=images,
                                                        z=z,
                                                        is_training=is_training)

    # Calculate loss
    # g_Loss, d_TrueLoss, d_FalseLoss = model.loss(dTrueLogit, dFalseLogit)
    g_Loss, d_Loss = model.loss(dFalseLogit, dTrueLogit)

    # Build a Graph that trains the model with one batch of examples and update sht model
    # parameters
    # d_Total_Loss = d_TrueLoss + d_FalseLoss
    g_train_op1, d_train_op1 = model.train(g_Loss, global_step, discriminator_total_loss=d_Loss)
    
    # images2, z2 = data.training_inputs()
    
    g_output2, dFalseLogit2 = model.inference(z=z,
                                              is_training=is_training,
                                              generator_reuse=True)

    g_Loss2 = model.loss(dFalseLogit2, generator_reuse=True)
    g_train_op2 = model.train(generator_total_loss=g_Loss2,
                              global_step=global_step,
                              reuse_optimizer=True)

    class LoggerHook(tf.train.SessionRunHook):
      """
        Logs loss and runtime.
      """
      def begin(self):
        self._step = -1
        self._start_time = time.time()

      def before_run(self, run_context):
        if (self._step % 2 == 0):  
          sess_args = tf.train.SessionRunArgs([g_Loss, d_Loss, global_step])
        else:
          sess_args = tf.train.SessionRunArgs([g_Loss2, global_step])

        return sess_args 

      def after_run(self, run_context, run_values):
        if self._step % FLAGS.log_frequency == 0 or self._step % FLAGS.log_frequency == 1:
          current_time = time.time()
          duration = current_time - self._start_time
          self._start_time = current_time
          loss_values = run_values.results
          examples_per_sec = FLAGS.log_frequency * FLAGS.batch_size / duration
          sec_per_batch = float(duration / FLAGS.log_frequency)

          if (self._step % 2 == 0):
            format_str = ('%s: step %d, generator loss1 = %.10f discriminator loss = %.10f (%.1f examples/sec; %.3f) sec/batch')
            print(format_str % (datetime.now(), self._step, loss_values[0], loss_values[1], examples_per_sec, sec_per_batch))
          else:
            print('%s step %d, generator loss2 = %.10f' % (datetime.now(), self._step, loss_values[0]))

        self._step = self._step + 1

    config = tf.ConfigProto(log_device_placement=FLAGS.log_device_placement)
    config.gpu_options.allocator_type = 'BFC'
    config.gpu_options.allow_growth = True

    saver = tf.train.Saver()
    # summary_writer = tf.summary.FileWriter(FLAGS.checkpoint_dir, g)
    summary_op = tf.summary.merge_all()
    saver_hook = tf.train.CheckpointSaverHook(checkpoint_dir=FLAGS.checkpoint_dir, save_steps=500, saver=saver)
    summary_hook = tf.train.SummarySaverHook(save_steps=500, output_dir=FLAGS.checkpoint_dir, summary_op=summary_op)

    idx = 0
    with tf.train.MonitoredTrainingSession(
        # checkpoint_dir=FLAGS.checkpoint_dir,
        hooks=[tf.train.StopAtStepHook(last_step=FLAGS.max_steps),
               tf.train.NanTensorHook([g_Loss]),
               tf.train.NanTensorHook([d_Loss]),
               LoggerHook(),
               saver_hook,
               summary_hook
             ],
        config=config) as mon_sess:
      while not mon_sess.should_stop():
        batch_z = np.random.uniform(-1,
                                    1,
                                    [FLAGS.batch_size, FLAGS.random_input_size]).astype(np.float32)
        data = dataset_files(FLAGS.dataset)
        batch_files = data[idx * FLAGS.batch_size: (idx + 1) * FLAGS.batch_size]
        batch = [get_image(batch_file, 64, is_crop=False) for batch_file in batch_files]
        batch_images = np.array(batch).astype(np.float32)
        mon_sess.run([g_train_op1, d_train_op1], feed_dict={z:batch_z, images:batch_images})
        mon_sess.run([g_train_op2], feed_dict={z:batch_z, images:batch_images})
        idx += 1
        if (idx == 205):
          idx = 0

def eval_once(saver, summary_writer, generated_images, z, summary_op):
  """
    Run eval once.

    Args:
      saver: Saver
      summary_writer: Summary Writer.
      generated_images : Images generated from generator of the model
      summary_op: Summary op
  """
  with tf.Session() as sess:
    ckpt = tf.train.get_checkpoint_state(FLAGS.checkpoint_dir)

    if ckpt and ckpt.model_checkpoint_path:
      # Restore from checkpoint
      saver.restore(sess, ckpt.model_checkpoint_path)

      # Extract global_step from it
      global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
    else:
      print('No checkpoint file found')
      return

    # Start the queue runners.
    coord = tf.train.Coordinator()
    try:
      threads = []
      for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
        threads.extend(qr.create_threads(sess, coord=coord, daemon=True, start=True))

      # images, zs = sess.run([generated_images, z])
        # np.save('z.npy', zs)
      batch_z = np.random.uniform(-1, 1,
                                  [FLAGS.batch_size, FLAGS.random_input_size]).astype(np.float32)
      images = sess.run([generated_images], feed_dict={z:batch_z})
      images = np.array(images[0]).astype(np.float32)
      image_save_filename = os.path.join(FLAGS.eval_dir, 'Image_' + str(0) + '.png')
      save_images(images, [8, 8], image_save_filename)
      i = 0
      # for image in images:
        # image_min = np.min(image)
        # image_max = np.max(image)
        # image = (image - image_min) / (image_max - image_min) * 255.0
        # print('Image Shape : {} Image min : {} Image max : {}'.format(image.shape, image_min, image_max))
        # image_save_filename = os.path.join(FLAGS.eval_dir, 'Image_' + str(i) + '.png')
        # cv2.imwrite(image_save_filename, image)
        # i = i + 1
        # imsave(inverse_transform(image), [8, 8], image_save_filename)

      # summary = tf.Summary()
      # summary.ParseFromString(sess.run(summary_op))
      # summary_writer.add_summary(summary, global_step)
    except Exception as e:  # pylint: disable=broad-except
      coord.request_stop(e)

    coord.request_stop()
    coord.join(threads, stop_grace_period_secs=10)

def eval(data, model):
  with tf.Graph().as_default() as g:

    is_training = tf.constant(False, name='is_training')
    # Get batch data
    # z = data.eval_inputs()
    z = tf.placeholder(tf.float32, [FLAGS.batch_size, FLAGS.random_input_size], name='z2')

    # Build a graph that generates the images from model
    generated_images = model.generator(z, is_training)

    # Restore the moving average version of the learned variables for eval
    variable_averages = tf.train.ExponentialMovingAverage(FLAGS.moving_average_decay)
    variables_to_restore = variable_averages.variables_to_restore()
    # saver = tf.train.Saver(variables_to_restore)
    saver = tf.train.Saver()

    # Build the summary operation based on the TF Collection of summaries
    summary_op = tf.summary.merge_all()

    summary_writer = tf.summary.FileWriter(FLAGS.eval_dir, g)

    while True:
      eval_once(saver, summary_writer, generated_images, z, summary_op)
      if FLAGS.run_once:
        break;

      time.sleep(FLAGS.eval_interval_secs)

def main(argv=None):
  data = DCGANData(FLAGS)
  model = DCGAN(FLAGS)

  if FLAGS.train:
    train(data, model)
  else:
    eval(data, model)

if __name__ == '__main__':
  tf.app.run()
