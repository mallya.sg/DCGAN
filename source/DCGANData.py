from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import cv2
import numpy as np
import os
import sys
import tensorflow as tf

class DCGANData(object):
  """
    Class holding the data for segnet.
  """
  def __init__(self, FLAGS):
    self.images = []
    self.imageLoaded = False
    self.image_shape = None
    self.FLAGS = FLAGS

  def int64_feature(self, value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

  def bytes_feature(self, value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

  def loadImage(self, loadDataFromNPZ=True):
    if not loadDataFromNPZ:
      # List the files in the directory
      print('Path : ', os.path.join(self.FLAGS.data_dir, self.FLAGS.image_dir))
      imageFileList = os.listdir(os.path.join(self.FLAGS.data_dir, self.FLAGS.image_dir))
      print('ImageFileList Size : ', len(imageFileList))
      try:
        # Load the image data
        index = 0
        for imageFileName in imageFileList:
          image = cv2.imread(os.path.join(self.FLAGS.data_dir,
                                          self.FLAGS.image_dir,
                                          imageFileName))
          image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

          self.images.append(image)
          index += 1
          if index % 5 == 0:
            print('Finished loading %d images' % (index))
      except:
        e = sys.exc_info()[0]
        print("Error : %s", e)

      self.images = np.array(self.images)

      saveFileName = os.path.join(self.FLAGS.data_dir, 'data.npz')
      np.savez(saveFileName, image = self.images)
    else:
      print(os.path.join(self.FLAGS.data_dir, 'data.npz'))
      saveFileName = os.path.join(self.FLAGS.data_dir, 'data.npz')
      dataDictionary = np.load(saveFileName)

      self.images = dataDictionary['images']

    self.imageLoaded = True
    print('Number of images : ', self.images.shape)

  def convertDataToRecords(self):
    if not os.path.exists(os.path.join(self.FLAGS.data_dir, 'train.tfrecords')):
      if not os.path.exists(os.path.join(self.FLAGS.data_dir, 'data.npz')):
        self.loadImage(loadDataFromNPZ=False)
      else:
        self.loadImage(loadDataFromNPZ=True)

      numberOfTrainSamples = self.images.shape[0]
      rows = self.images.shape[1]
      cols = self.images.shape[2]
      depth = self.images.shape[3]

      self.image_shape = (rows, cols, depth)

      filename = os.path.join(self.FLAGS.data_dir, 'train.tfrecords')
      print('Writing ', filename)

      writer = tf.python_io.TFRecordWriter(filename)
      for index in range(numberOfTrainSamples):
        image_raw = self.images[index].tostring()
        example = tf.train.Example(features=tf.train.Features(feature={
          'height' : self.int64_feature(rows),
          'width'  : self.int64_feature(cols),
          'depth'  : self.int64_feature(depth),
          'image_raw' : self.bytes_feature(image_raw)
        }))
        writer.write(example.SerializeToString())
      writer.close()
    else:
      print('Train records already exist')

  def read_and_decode(self, filename_queue):
    reader = tf.TFRecordReader()

    _, serialized_example = reader.read(filename_queue)

    features = tf.parse_single_example(
      serialized_example,
      # Defaults are not specified since both keys are required.
      features={
        'height'   : tf.FixedLenFeature([], tf.int64),
        'width'    : tf.FixedLenFeature([], tf.int64),
        'depth'    : tf.FixedLenFeature([], tf.int64),
        'image_raw': tf.FixedLenFeature([], tf.string),
      }
    )

    # Convert from a scalar string tensor (whose single string has length
    # FLAGS.image_height * FLAGS.image_width * FLAGS.image_depth) to a uint8 tensor with shape
    # [FLAGS.image_height, FLAGS.image_width, FLAGS.image_depth]
    image = tf.decode_raw(features['image_raw'], tf.uint8)

    height = tf.cast(features['height'], tf.int32)
    width = tf.cast(features['width'], tf.int32)
    depth = tf.cast(features['depth'], tf.int32)

    image_shape = tf.stack([self.FLAGS.image_height, self.FLAGS.image_width, self.FLAGS.image_depth])

    image = tf.reshape(image, image_shape)

    return image

  def generate_image_and_z_batch(self, image, min_queue_examples, shuffle):
    """
      Construct a queued batch of images and labels.

      Args:
        image: 3-D Tensor of [height, width, 3] of type.float32.
        min_queue_examples: int32, minimum number of samples to retain in the queue that
                            provides of batches of examples.
        shuffle: boolean indicating whether to use a shuffling queue

      Returns:
        images: Images. 4D tensor of [batch_size, height, width, depth] size.
    """
    # Create a queue that shuffles the examples, and then read batch_size images + labels
    # from the example queue.
    num_preprocess_threads = 8
    dim = self.FLAGS.random_input_size
    z = tf.random_uniform(
      shape=[self.FLAGS.random_input_size],
      minval=-1,
      maxval=1)

    ## z = tf.truncated_normal(
    ##   shape=[self.FLAGS.random_input_size],
    ##   mean=0,
    ##   stddev=1.0
    ##   )

    if shuffle:
      images, zs = tf.train.shuffle_batch(
        [image, z],
        batch_size=self.FLAGS.batch_size,
        num_threads=num_preprocess_threads,
        capacity=min_queue_examples + 3 * self.FLAGS.batch_size,
        min_after_dequeue=min_queue_examples)
    else:
      images, zs = tf.train.batch(
        [image, z],
        batch_size=self.FLAGS.batch_size,
        num_threads=num_preprocess_threads,
        capacity=min_queue_examples + 3 * self.FLAGS.batch_size
      )

    tf.summary.image('images', images)

    return images, zs

  def training_inputs(self):
    if not os.path.exists(os.path.join(self.FLAGS.data_dir, 'train.tfrecords')):
      self.convertDataToRecords()

    if not self.FLAGS.num_epochs:
      self.FLAGS.num_epochs = None

    filename = os.path.join(self.FLAGS.data_dir,
                            self.FLAGS.train_file)

    filename_queue = tf.train.string_input_producer(
      [filename], num_epochs=self.FLAGS.num_epochs)

    # Even when reading in multiple threads, share the filename queue
    image_batch = self.read_and_decode(filename_queue)

    reshaped_image_batch = tf.cast(image_batch, tf.float32)

    # Subtract off the mean and divide by the standard deviation of the pixels
    float_image_batch = tf.image.per_image_standardization(reshaped_image_batch)

    # NOTE : Check what happens if we do not set the shape here
    float_image_batch.set_shape(
      [self.FLAGS.image_height, self.FLAGS.image_width, self.FLAGS.image_depth])

    # Ensure that the random shuffling has good mixing properties.
    # min_fraction_of_examples_in_queue = 0.1
    min_queue_examples = int(
      self.FLAGS.num_examples_per_epoch_for_training * self.FLAGS.min_fraction_of_examples_in_queue)

    print('Filling queue with %d LFW images before starting to train. '
          'This will take a few minutes.' % min_queue_examples)

    return self.generate_image_and_z_batch(float_image_batch,
                                           min_queue_examples,
                                           shuffle=True)
    # # The op for initializing the variables
    # init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    #
    # ith tf.Session() as sess:
    #  sess.run(init_op)
    #
    #  coord = tf.train.Coordinator()
    #
    #  threads = tf.train.start_queue_runners(coord=coord)
    #
    #  # Read 3 batches for example
    #  for i in xrange(3):
    #    im = np.array(sess.run([image_batch]))
    #    im = im[0, :, :, :]
    #    im = im.transpose((1, 2, 0))
    #    print('Current Batch')
    #    print('Image : ', np.array(im).shape)
    #    im = cv2.cvtColor(im, cv2.COLOR_RGB2BGR)
    #    cv2.imwrite('Image' + str(i) + '.png', im)

  def eval_inputs(self):
    # Create a queue that shuffles the examples, and then read batch_size images + labels
    # from the example queue.
    num_preprocess_threads = 8
    z = tf.random_uniform(shape=[self.FLAGS.random_input_size], minval=-1, maxval=1)

    zs = tf.train.batch(
        [z],
        batch_size=self.FLAGS.batch_size,
        num_threads=num_preprocess_threads,
        capacity=3 * self.FLAGS.batch_size
      )

    return zs
